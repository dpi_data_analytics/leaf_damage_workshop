# basic commands

## Activate the env 
```
conda activate maskrcnn
```   
## split the video into frames
```
ffmpeg -i PXL_20210902_202430217.mp4 cat2/%04d.png
```   
## Resize the images to 320 x 240
```
python resize_images_in_folder.py cat2 320 240
```

## writes to a folder called out
I modified visulise.py to write to file
```
for cat in cat/*.png; do python maskrcnn_demo.py $cat; done
```
## change into the directory and join the video back together 
```
cd out
ffmpeg -framerate 5 -start_number 50 -i %04d.png -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4
```