# https://www.youtube.com/watch?v=tk9war7_y0Q
import cv2
import numpy as np
import utlis

# a4 background
## 
webcam = False
#path = "test_images/leaf.jpg"
path = "test_images/apple.jpg"
cap = cv2.VideoCapture(0)
# settings brightness
cap.set(10, 160)
# settings width 3
cap.set(3,1920)
# height 4 
cap.set(4,1080)

scale_factor = 3
width_paper = 210*scale_factor
height_paper = 297*scale_factor

while True:
    if webcam:
        success, img = cap.read()
    else:
        img = cv2.imread(path)
    img_contours, conts = utlis.get_contours(img, show_canny=False, 
                                             min_area=50000,
                                             draw=False)
    if len(conts) != 0:
        # approx is four points
        biggest = conts[0][2]
        img_warp = utlis.warp_img(img, biggest, width_paper,
                                  height_paper)
        img_contours2, conts2 = utlis.get_contours(img_warp, show_canny=False,
                                              min_area=2000,
                                              cThr=[50,50],
                                              draw=False)

        if len(conts) != 0:
            for obj in conts:
                cv2.polylines(img_contours, [obj[2]], True, (0,255,0), 2)
                # not working
                #new_points = utlis.reorder(obj[2])
                # width in mm
                #print(utlis.find_distance(new_points[0][0]//scale, 
                #                    new_points[1][0]//scale))
                #print(utlis.find_distance(new_points[0][0]//scale, 
                #                    new_points[2][0]//scale))

        cv2.imshow('A4', img_warp)

        cv2.imshow('A4', img_contours2)
    if len(conts) != 0:
        # resize for display only
        img = cv2.resize(img, (0,0), None, 0.5,0.5)
        cv2.imshow('original', img)
        cv2.waitKey(1)
        # 297x210
