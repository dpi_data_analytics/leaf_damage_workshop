import cv2
import numpy as np

def get_contours(img, cThr=[100,100], show_canny=False, min_area=1000,
                filter=0, draw=False):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blur
    img_blur = cv2.GaussianBlur(img_gray, (5,5), 1)
    # canny edge detetor
    img_canny = cv2.Canny(img_blur, cThr[0], cThr[1])
    kernal = np.ones((5,5))
    img_dial = cv2.dilate(img_canny, kernal, iterations=3)
    img_thre = cv2.erode(img_dial, kernal, iterations=2)
    if show_canny:
        cv2.imshow('Canny', img_thre)
    # contours
    contours, hiearchy = cv2.findContours(img_thre, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # 
    final_contours = []
    for i in contours:
        area = cv2.contourArea(i)
        if area > min_area:
            param = cv2.arcLength(i, True)
            # corners
            approx = cv2.approxPolyDP(i, 0.02*param, True)
            bbox = cv2.boundingRect(approx)
            if filter > 0:
                if len(approx) == filter:
                    final_contours.append([len(approx), area, approx,
                                          bbox, i])
            else:
                final_contours.append([len(approx), area, approx,
                                          bbox, i])
    final_contours = sorted(final_contours, key = lambda x: x[1], reverse=True)
    if draw:
        for con in final_contours:
            cv2.drawContours(img, con[4], -1, (0,0,255), 3)

    return img, final_contours

def reorder(my_points):
    print(my_points.shape)
    my_points_new = np.zeros_like(my_points)
    my_points = my_points.reshape((4,2))
    # smallest element 0, max will be corner w and h
    add = my_points.sum(1)
    my_points_new[0] = my_points[np.argmin(add)]
    my_points_new[3] = my_points[np.argmax(add)]
    diff = np.diff(my_points, axis=1)
    my_points_new[1] = my_points[np.argmin(diff)]
    my_points_new[2] = my_points[np.argmax(diff)]
    return my_points_new

# function to use 4 points to warp image so that we can work out corner orin
def warp_img(img, points, w, h, pad=20):
    points = reorder(points)
    pts1 = np.float32(points)
    pts2 = np.float32([[0,0],[w, 0], [0,h],[w,h]])
    matrix = cv2.getPerspectiveTransform(pts1, pts2)
    img_warp = cv2.warpPerspective(img, matrix, (w, h))
    img_warp = img_warp[pad:img_warp.shape[0]-pad, pad:img_warp.shape[1]-pad]
    return img_warp

def find_distance(pts1, pts2):
    return ((pts2[0] - pts1[0])**2 + (pts2[1] - pts1[1])**2)**0.5
