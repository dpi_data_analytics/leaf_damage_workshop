# DPI leaf damage workshop  

## OpenCV  
See in `/home/dpidave/projects/leaf_damage_workshop/opencv/youtube-projects-master/OpenCV/camruler`. 

1. Find out your device for the webcam with `ls -1 /dev/vid*`, this will return a video number, mine was `video0`. 
2. Start the application, providing the video number (ie 0 in my case), `python camruler.py 1`.  
3. This should open up the camera GUI, showing the image with commands along the left hand side of the screen.  
4. If you need to rotate the image hit `R`.  
5. Now we will configure OpenCV so that it knows how many pixels equate to a specific number of mm. To do this hit `C` to enter config mode.  
6. Place a ruler running along the diagonal with the 0 mark on the center cross hairs, run the ruler ticks along the diagonal red line.  
7. Determine the length of the diagonal, ie the field of vision from the center to the corner, write this down.  
8. Edit line 72 `cal_range = 110` to show your value.  
9. Quit and restart the `camruler.py` script as before.  
10. Now hit `C` again, then click on 5mm, follow the instructions on screen clicking on each ruler tick in 5mm increments until you get to the edge of the screen again.  
11. After this it should be calibrated. You can measure using the left click on the mouse, to clear the input hit the right click.  
12. Push `A` to enter auto mode and it should automatically measure images.  
13. If you need to change the sensitivity threshold, hit `T` and then drag your mouse back and forth across the screen to change the threshold (change will be indicated by threshold value).  
14. When you have found the right threshold, hit `T` again to lock in this value.  
15. If you have issue with light, hit `N` to normalise, once again use the mouse cursor moving around the screen to change the value.  

## Training a tensorflow model to detect leaf damage.  

Based on the excellent tutorial https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html#evaluating-the-model-optional. 

The training dataset was from https://github.com/digitalepidemiologylab/plantvillage_deeplearning_paper_dataset. I only used ~100 images from the `11` folder, these look like grape leaves with some kind of necrosis.  

1. Follow the steps in the tutorial to install the dependencies (some pain and suffering)   
2. Adjust the images to 640x640px using `scripts/scripts/resize_images_in_folder.py`  
```
python scripts/resize_images_in_folder.py leaf_damage/images/ 640 640
```
3. Use labelling (https://github.com/tzutalin/labelImg/releases/tag/v1.8.1) to annotate the leaf damage in each image; each JPG will now have an xml file.  
4. Split the JPG/XML pairs into trianing and test datasets ~10% testing using the `scripts/preprocessing/partition_dataset.py`.   
```
# python partition_dataset.py -x -i C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/images -r 0.1
```
5. Create a key file in the annotations directory.    
```
cat ml/workspace/leaf_damage/annotations/label_map.pbtxt 
item {
    id: 1
    name: 'damage'
}
```
6. Create tf records for the training and test sets.    
```
python generate_tfrecord.py -x C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/images/train -l C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/annotations/label_map.pbtxt -o C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/annotations/train.record
python generate_tfrecord.py -x C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/images/test -l C:/Users/sglvladi/Documents/Tensorflow2/workspace/training_demo/annotations/label_map.pbtxt -o C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/annotations/test.record
```
7. Modify the config file in `models/my_ssd_resnet50_v1_fpn/` so that the paths point in the correct location, reduce the batch size to 1.  
8. Run the training. 
```
python model_main_tf2.py --model_dir=models/my_ssd_resnet50_v1_fpn --pipeline_config_path=models/my_ssd_resnet50_v1_fpn/pipeline.config
```
9. Export the model.  
```
python exporter_main_v2.py --input_type image_tensor --pipeline_config_path ./models/my_ssd_resnet50_v1_fpn/pipeline.config --trained_checkpoint_dir models/my_ssd_resnet50_v1_fpn/ --output_directory ./exported-models/my_model
```
10. Use the notebook in `ml/results` to test the model.  

## ML  
Contains the scripts for the maskrcnn workflow for image segmentation. See the repo https://github.com/matterport/Mask_RCNN. I couldn't get this to work with a notebook so I just wrote python scripts. As always there will be pain and suffering with tensorflow versioning. 
